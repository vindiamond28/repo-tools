import React from 'react';

import {Snackbar, withStyles} from "@material-ui/core";
import {Cancel as CancelIcon, Add as AddIcon, Error as ErrorIcon, Close as CloseIcon} from '@material-ui/icons';
import {
    Chip,
    Fab,
    DialogActions,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField,
    IconButton,
    SnackbarContent
} from "@material-ui/core";

import clsx from "clsx";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {selectUser, unselectUser} from "./redux/actions";
import fetchUsersAction from "./redux/actions/fetchUsers";
import {API_PATH} from "./redux/constants";


class UserSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alertOpen: false,
            failedUser: ''
        };

        this.handleUserAdd = this.handleUserAdd.bind(this);
    }

    componentDidUpdate() {
        if (!this.props.selectedUser && this.props.users.length !== 0) {
            this.handleSelect(this.props.users[0]);
        }
    }

    handleSelect(i) {
        this.props.selectUser(i);
        this.props.selectHandler();
    }

    handleDelete(i) {
        fetch(API_PATH + '/users/' + i, {
            method: 'DELETE'
        })
            .then(this.props.fetchUsers);

        if (i === this.props.selectedUser)
            this.props.unselectUser();
    }

    handleUserAdd(e) {
        e.preventDefault();

        let name = (new FormData(e.target)).get('name');

        this.setState({alertOpen: false});

        fetch(API_PATH + '/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name})
        })
            .then(r => {
                if (!r.ok)
                    return this.setState({failedUser: name});

                this.props.fetchUsers();
                fetch(API_PATH + '/pipelines', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        user: name,
                        'gather-repos': true
                    })
                });
            });
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Snackbar
                    open={this.state.failedUser !== ''}
                    onClose={() => this.setState({failedUser: ''})}
                    autoHideDuration={5000}
                    anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
                >
                    <SnackbarContent
                        className={classes.error}
                        aria-describedby="client-snackbar"
                        message={
                            <span id="client-snackbar" className={classes.message}>
                        <ErrorIcon className={clsx(classes.icon, classes.iconVariant)}/>
                                    Error: user {this.state.failedUser} does not exist. Make sure you entered correct data.
                        </span>
                        }
                        action={[
                            <IconButton key="close" aria-label="close" color="inherit"
                                        onClick={() => this.setState({failedUser: ''})}>
                                <CloseIcon className={classes.icon}/>
                            </IconButton>,
                        ]}/>
                </Snackbar>

                {this.props.users && this.props.users.map((i, index) => (
                    <Chip
                        label={i}
                        key={index}
                        clickable
                        onDelete={() => this.handleDelete(i)}
                        onClick={() => this.handleSelect(i)}
                        className={classes.chip}
                        deleteIcon={<CancelIcon/>}
                        color={this.props.selectedUser === i ? 'primary' : undefined}
                    />
                ))}
                <Fab size="small" color="secondary" aria-label="Add" className={clsx(classes.fab, classes.alignRight)}
                     onClick={() => this.setState({alertOpen: true})}>
                    <AddIcon/>
                </Fab>
                <Dialog open={this.state.alertOpen} onClose={() => this.setState({alertOpen: false})}
                        aria-labelledby="form-dialog-title">
                    <form onSubmit={this.handleUserAdd}>
                        <DialogTitle id="form-dialog-title">Add user</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Enter username to add:
                            </DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name='name'
                                label="Username"
                                type="text"
                                fullWidth
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.setState({alertOpen: false})} color="primary">
                                Cancel
                            </Button>
                            <Button type='submit' color="primary">
                                Add
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = state => state;

function mapDispatchToProps(dispatch) {
    return {
        selectUser: user => dispatch(selectUser(user)),
        unselectUser: () => dispatch(unselectUser()),
        fetchUsers: bindActionCreators(fetchUsersAction, dispatch)
    };
}


const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'left',
        flexWrap: 'wrap',
        padding: theme.spacing(3, 2)
    },
    chip: {
        margin: theme.spacing(1)
        // padding: theme.spacing(1)
    },
    fab: {
        margin: theme.spacing(1),
        marginTop: 3
    },
    alignRight: {
        marginRight: 12,
        marginLeft: 'auto'
    },
    error: {
        backgroundColor: theme.palette.error.dark,
        boxShadow: '0 0 12px 1px #d32f2f',
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserSelect));
