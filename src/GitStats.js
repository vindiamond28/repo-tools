import React from 'react';

import {
    Checkbox,
    Chip,
    Container,
    Divider,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    Fab,
    FormControl,
    FormControlLabel,
    FormGroup,
    Grid,
    InputLabel,
    Link,
    makeStyles,
    MenuItem,
    Paper,
    Select as MUISelect,
    TextField,
    Typography,
    useTheme,
    withStyles,
    Fade,
    Zoom
} from "@material-ui/core";
import {Rating} from '@material-ui/lab';
import {
    Cancel as CancelIcon,
    ExpandMore as ExpandMoreIcon,
    SwapVert as SwitchIcon,
    MergeType as ArrowMergeIcon
} from "@material-ui/icons";
import {emphasize} from "@material-ui/core/styles";
import {grey} from "@material-ui/core/colors";

import Select from "react-select/creatable";

import {connect} from "react-redux";

import moment from "moment";
import clsx from 'clsx';

import {setBranches, setCtx, setLang, toggleFavourite, toggleKeys, toggleReset, unselectUser} from "./redux/actions";
import {API_PATH} from "./redux/constants";
import Loader from "react-dots-loader";
import 'react-dots-loader/index.css';

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

class GitStats extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            panelExpanded: false,
            pipeline: {},
            interval: undefined,
            loading: true,
            birds: []
        };

        this.fetchStatus = this.fetchStatus.bind(this);
        this.switchBranches = this.switchBranches.bind(this);
        this.toggleStar = this.toggleStar.bind(this);
        this.fetchSnapshot = this.fetchSnapshot.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedUser && this.state.pipeline !== undefined && !this.state.pipeline[this.props.selectedUser])
            this.fetchStatus();

        if (prevProps.selectedUser !== this.props.selectedUser)
            this.setState({birds: []});
    }

    fetchSnapshot() {
        fetch(API_PATH + '/snapshots/0?user=' + this.props.selectedUser, {
            headers: {
                Accept: 'application/json'
            }
        })
            .then(r => r.json())
            .then(r => {
                let starsInStorage = JSON.parse(window.localStorage.getItem('stars'));

                if (!starsInStorage[this.props.selectedUser])
                    starsInStorage[this.props.selectedUser] = [];

                r.timestamp = r.timestamp * 1000 || 0;
                r.groups = r.groups || {};

                Object.values(r.groups).map(group =>
                    Object.values(group).map((repo, j) => {
                        repo.fav = starsInStorage[this.props.selectedUser].includes(Object.keys(group)[j]);
                    })
                );

                let state = this.state;
                    state.pipeline[this.props.selectedUser].snapshot = r;
                    this.setState(state);
            });
    }

    fetchStatus() {
        fetch(API_PATH + '/statuses/0?user-id=' + this.props.selectedUser, {
            headers: {
                Accept: 'application/json'
            }
        })
            .then(r => r.json())
            .then(r => {
                r.logs = r.logs.replace('\n', '<br />');

                let state = this.state;
                state.pipeline[this.props.selectedUser] = r;

                if (r.status === 'pending' && !this.state.interval)
                    state = {
                        ...state,
                        panelExpanded: true,
                        loading: true,
                        interval: setInterval(this.fetchStatus, 3000)
                    };

                if (r.status !== 'pending') {
                    state.loading = false;
                    this.fetchSnapshot();
                }

                if (r.status !== 'pending' && this.state.interval)
                    state = {
                        ...state,
                        panelExpanded: false,
                        interval: clearInterval(this.state.interval)
                    };

                this.setState(state);
            });
    }

    toggleStar(name, group) {
        let newState = this.state,
            starsInStorage = JSON.parse(window.localStorage.getItem('stars'));

        if (!starsInStorage[this.props.selectedUser])
            starsInStorage[this.props.selectedUser] = [];

        if (starsInStorage[this.props.selectedUser].includes(name))
            starsInStorage[this.props.selectedUser].remove(name);
        else
            starsInStorage[this.props.selectedUser].push(name);

        newState.pipeline[this.props.selectedUser].snapshot.groups[group][name].fav = !newState.pipeline[this.props.selectedUser].snapshot.groups[group][name].fav;

        window.localStorage.setItem('stars', JSON.stringify(starsInStorage));
        this.setState(newState);
    }

    toggleBird(name) {
        let newState = this.state;

        this.state.birds.includes(name) ?
            newState.birds.remove(name) :
            newState.birds.push(name);

        this.setState(newState);
    }

    switchBranches(refreshOnly) {
        let data;

        if (!refreshOnly) {
            if (this.props.branches === null)
                data = [];
            else
                data = this.props.branches.slice();

            data.reverse();
            data = data.map(i => i.value);
            data = {
                user: this.props.selectedUser,
                // reset: this.props.reset,
                // prefetch: this.props.prefetch,
                branches: data,
                lang: this.props.language,
                ctx: this.props.context,
                updKeys: this.props.updKeys,
                birds: this.state.birds
            };
            data = JSON.stringify(data);
        }

        let newState = {panelExpanded: true};

        fetch(API_PATH + '/pipelines', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: refreshOnly ? "{}" : data
        })
            .then(() => {
                setTimeout(() => {
                    this.fetchStatus();
                }, 5000);

                newState.loading = true;

                this.setState(newState);
            });
    }

    render() {
        const {classes, selectedUser} = this.props;

        return (
            <div>
                <div className={classes.withPadding}>
                    <IntegrationReactSelect/>
                    {/*<FormGroup row>*/}
                    {/*    <FormControlLabel*/}
                    {/*        control={*/}
                    {/*            <Checkbox checked={this.props.reset} onChange={this.props.toggleReset}/>*/}
                    {/*        }*/}
                    {/*        labelPlacement={'start'}*/}
                    {/*        label="Reset"*/}
                    {/*    />*/}
                    {/*</FormGroup>*/}
                    <FormGroup row>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="ctx">Context</InputLabel>
                            <MUISelect
                                value={this.props.context}
                                onChange={this.props.setCtx}
                                inputProps={{
                                    id: 'ctx',
                                }}
                                className={classes.selectEmpty}
                            >
                                <MenuItem selected value='null'>
                                    -
                                </MenuItem>
                                <MenuItem value='dk'>DK</MenuItem>
                                <MenuItem value='sv'>SV</MenuItem>
                            </MUISelect>
                        </FormControl>

                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="lang">Language</InputLabel>
                            <MUISelect
                                value={this.props.language}
                                onChange={this.props.setLang}
                                inputProps={{
                                    id: 'lang',
                                }}
                                className={classes.selectEmpty}
                            >
                                <MenuItem value={'null'}>
                                    -
                                </MenuItem>
                                <MenuItem value='en'>EN</MenuItem>
                                <MenuItem value='dk'>DK</MenuItem>
                                <MenuItem value='sv'>SV</MenuItem>
                            </MUISelect>
                        </FormControl>

                        <FormControlLabel
                            control={
                                <Checkbox checked={this.props.updKeys} onChange={this.props.toggleKeys}/>
                            }
                            labelPlacement={'start'}
                            label="Update keys"
                        />
                    </FormGroup>

                    <Fab variant="extended" color='secondary' className={clsx(classes.fab, classes.alignRight)}
                         style={{display: "flex"}}
                         onClick={this.switchBranches}>
                        <SwitchIcon className={classes.extendedIcon}/>
                        Process
                    </Fab>
                    {this.state.pipeline[selectedUser] && (
                        <ExpansionPanel expanded={this.state.panelExpanded} onChange={() => {
                            this.setState({panelExpanded: !this.state.panelExpanded})
                        }}>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon/>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography
                                    className={classes.heading}>Status: {this.state.pipeline[selectedUser].status}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails
                                style={{background: grey["900"], maxHeight: '50vh', overflowX: 'auto'}}>
                                <div className={classes.console}>
                                    <Typography
                                        dangerouslySetInnerHTML={{__html: this.state.pipeline[selectedUser].logs}}/>
                                    <Loader size={9} distance={8} visible={this.state.loading}/>
                                </div>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    )}

                    <Divider style={{marginTop: 15}}/>

                    {this.state.pipeline[selectedUser] && <Container style={{maxWidth: '100%'}}>
                        <Typography variant={'body1'} style={{padding: '12px 0', display: "flex"}}>
                            <span>
                                {/*/выпилено/*/}
                                {/*{this.state.pipeline[selectedUser].lastSwitch.map((branch, i) => (*/}
                                {/*    <Typography key={i}*/}
                                {/*                variant={"button"}>{branch}{this.state.pipeline[selectedUser].lastSwitch[i + 1] && ", "}</Typography>*/}
                                {/*))}*/}
                                <Rating
                                    name="simple-controlled"
                                    value={this.props.onlyFav ? 1 : 0}
                                    max={1}
                                    onClick={(e) => {
                                        e.target.nodeName === 'INPUT' && this.props.toggleFavourite()
                                    }}
                                />

                            </span>
                            <span className={classes.alignRight}>
                            Snapshot made {moment(this.state.pipeline[selectedUser].snapshot.timestamp).format('D/MM HH:mm')}
                                <Link style={{marginLeft: 12}}
                                      href={'#'} onClick={() => this.switchBranches(true)} className={classes.link}>
                                Refresh
                            </Link>

                        </span>
                        </Typography>
                    </Container>
                    }

                    <Divider/>

                    {this.state.pipeline[selectedUser] && window.Object.keys(this.state.pipeline[selectedUser].snapshot.groups).map((group, key) => (
                        <section key={key}>
                            <Typography variant={'h5'} style={{marginTop: 18}}>{group.capitalize()}</Typography>
                            {/* Loop with repo names */}
                            {window.Object.keys(this.state.pipeline[selectedUser].snapshot.groups[group]).map((item, i) => {
                                if (this.props.onlyFav && !this.state.pipeline[selectedUser].snapshot.groups[group][item].fav)
                                    return [];
                                else
                                    return (
                                        <div key={i} style={{marginTop: 8}}>
                                            <Zoom in>
                                                <Grid spacing={1} container>
                                                    <Grid item xs={1}>
                                                        <Checkbox
                                                            checked={this.state.birds.includes(item)}
                                                            onChange={(e) => {
                                                                e.target.nodeName === 'INPUT' && this.toggleBird(item)
                                                            }}
                                                            // value="checkedA"
                                                            inputProps={{
                                                                'aria-label': 'primary checkbox',
                                                            }}
                                                        />
                                                    </Grid>

                                                    <Grid item xs={1} className={classes.centerGrid}>
                                                        <Rating
                                                            value={this.state.pipeline[selectedUser].snapshot.groups[group][item].fav ? 1 : 0}
                                                            max={1}
                                                            name={item + '_star'}
                                                            onClick={(e) => {
                                                                e.target.nodeName === 'INPUT' && this.toggleStar(item, group)
                                                            }}
                                                        />
                                                    </Grid>

                                                    <Grid item xs={4} className={classes.centerGrid}>
                                                        <Typography variant={'body1'}>
                                                            {item.capitalize()}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={4} className={classes.centerGrid}>
                                                        <ArrowMergeIcon/>
                                                        <Typography variant={'body1'}>
                                                            {this.state.pipeline[selectedUser].snapshot.groups[group][item].branch}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Divider/>
                                                    </Grid>
                                                </Grid>
                                            </Zoom>
                                        </div>
                                    );
                            })}
                        </section>
                    ))}
                </div>
            </div>
        );
    }
}

// function customSort(values, keys, br) {
//     var len = values.length, result = {};
//
//     for (var i = 0; i < len; i++) {
//         for (var j = 0; j < len - i - 1; j++) {
//             if (values[j]['default-branch'] !== br && values[j + 1].branch === br) {
//                 var temp = values[j],
//                     temp1 = keys[j];
//
//                 values[j] = values[j + 1];
//                 keys[j] = keys[j + 1];
//                 values[j + 1] = temp;
//                 keys[j + 1] = temp1;
//             }
//         }
//     }
//
//     for (let i = 0; i < len; i++) {
//         result[keys[i]] = values[i];
//     }
//
//     return result;
// }

window.String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1)
};

const styles = theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'justify',
        marginTop: theme.spacing(3),
        width: '100%',
        overflowX: 'auto',
        marginBottom: theme.spacing(2),
        // color: theme.palette.text.secondary,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary
    },
    bold: {
        fontWeight: 500
    },
    active: {
        background: theme.palette.primary.main,
        color: '#fff'
    },
    marginTop: {
        marginTop: theme.spacing(4)
    },
    withPadding: {
        padding: theme.spacing(3, 2)
    },
    borderLeft: {
        borderLeft: '1px solid #ccc'
    },
    fab: {
        margin: theme.spacing(1),
        height: theme.spacing(5)
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    console: {
        fontFamily: '"DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace',
        whiteSpace: 'pre-wrap',
        wordBreak: 'normal',
        color: '#c4c4c4',
        fontSize: 12
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
        width: 160
    },
    table: {
        minWidth: 650,
    },
    link: {
        margin: theme.spacing(.5)
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0
    },
    alignRight: {
        marginRight: 12,
        marginLeft: 'auto'
    },
    centerGrid: {
        alignItems: 'center',
        display: 'flex'
    }
});


const mapStateToProps = state => state;

function mapDispatchToProps(dispatch) {
    // noinspection JSUnresolvedFunction
    return {
        unselectUser: () => dispatch(unselectUser()),
        setBranches: e => dispatch(setBranches(e)),
        toggleReset: () => dispatch(toggleReset()),
        toggleKeys: () => dispatch(toggleKeys()),
        toggleFavourite: () => dispatch(toggleFavourite()),
        setLang: e => dispatch(setLang(e.target.value)),
        setCtx: e => dispatch(setCtx(e.target.value))
    };
}

//Multiple select stuff
const suggestions = [
    'master',
    'production',
    'CP-230',
    'UN-130',
    'UN-135'
].map(suggestion => ({
    value: suggestion,
    label: suggestion,
}));

const useStyles = makeStyles(theme => ({
    input: {
        display: 'flex',
        padding: 0,
        height: 'auto',
    },
    alignRight: {
        marginLeft: "auto",
        marginRight: -12
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    chip: {
        margin: theme.spacing(0.5, 0.25),
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08,
        ),
    },
    noOptionsMessage: {
        padding: theme.spacing(1, 2),
    },
    placeholder: {
        position: 'absolute',
        left: 2,
        bottom: 6,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    }
}));

function NoOptionsMessage(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function inputComponent({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

function Control(props) {
    const {
        children,
        innerProps,
        innerRef,
        selectProps: {classes, TextFieldProps},
    } = props;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps,
                },
            }}
            {...TextFieldProps}
        />
    );
}

function Option(props) {
    return (
        <MenuItem
            ref={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontWeight: props.isSelected ? 500 : 400,
            }}
            {...props.innerProps}
        >
            {props.children}
        </MenuItem>
    );
}

function Placeholder(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function ValueContainer(props) {
    return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
    return (
        <Fade timeout={{enter: 700}} in><Chip
            tabIndex={-1}
            label={props.children}
            className={clsx(props.selectProps.classes.chip, {
                [props.selectProps.classes.chipFocused]: props.isFocused,
            })}
            color={'primary'}
            onDelete={props.removeProps.onClick}
            deleteIcon={<CancelIcon {...props.removeProps} />}
        /></Fade>
    );
}

function Menu(props) {
    return (
        <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
}

const components = {
    Control,
    Menu,
    MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    ValueContainer
};

function IntegrationReactSelect(props) {
    const classes = useStyles();
    const theme = useTheme();

    function handleChangeMulti(value) {
        props.setBranches(value);
    }

    const selectStyles = {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
    };

    return (
        <Select
            classes={classes}
            styles={selectStyles}
            inputId="react-select-multiple"
            TextFieldProps={{
                label: '',
                InputLabelProps: {
                    htmlFor: 'react-select-multiple',
                    shrink: true,
                },
            }}
            placeholder="Type branches to checkout"
            options={suggestions}
            components={components}
            value={props.branches}
            onChange={handleChangeMulti}
            isMulti
        />
    );
}

// eslint-disable-next-line no-func-assign
IntegrationReactSelect = connect(mapStateToProps, mapDispatchToProps)(IntegrationReactSelect);
/**/

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(GitStats));
