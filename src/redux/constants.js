export const SELECT_USER = "SELECT_USER";

export const SET_BRANCHES = "SET_BRANCHES";
export const SET_LANG = "SET_LANG";
export const SET_CTX = "SET_CTX";

export const TOGGLE_RESET = "TOGGLE_RESET";
export const TOGGLE_KEYS = "TOGGLE_KEYS";

export const FETCH_INFO_PENDING = "FETCH_INFO_PENDING";
export const FETCH_INFO_SUCCESS = "FETCH_INFO_SUCCESS";
export const FETCH_INFO_ERROR = "FETCH_INFO_ERROR";

export const FETCH_USERS_PENDING = "FETCH_USERS_PENDING";
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
export const FETCH_USERS_ERROR = "FETCH_USERS_ERROR";

export const TOGGLE_FAV = "TOGGLE_FAV";

// export const API_PATH = '/repo-tools/api/v1';
export const API_PATH = 'http://dminik.cliodev.dk/repo-tools/api/v1';
