import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {createLogger} from "redux-logger";
import ReduxThunk from "redux-thunk";
import rootReducer from "./reducers";

const logger = createLogger();

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(ReduxThunk, logger)
    ));

export default store;