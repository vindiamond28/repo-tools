import {
    FETCH_INFO_ERROR,
    FETCH_INFO_PENDING,
    FETCH_INFO_SUCCESS,
    FETCH_USERS_ERROR,
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    SELECT_USER,
    SET_BRANCHES,
    SET_CTX,
    SET_LANG,
    TOGGLE_FAV,
    TOGGLE_KEYS,
    TOGGLE_RESET
} from "../constants";
import initialState from '../initialState';

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SELECT_USER:
            return {
                ...state,
                selectedUser: action.payload
            };

        case SET_BRANCHES:
            return {
                ...state,
                branches: action.payload
            };

        case TOGGLE_RESET:
            return {
                ...state,
                reset: !state.reset
            };

        case TOGGLE_KEYS:
            return {
                ...state,
                updKeys: !state.updKeys
            };

        case SET_CTX:
            return {
                ...state,
                context: action.payload
            };

        case SET_LANG:
            return {
                ...state,
                language: action.payload
            };

        case FETCH_INFO_PENDING:
            return {
                ...state,
                pending: true
            };

        case FETCH_INFO_SUCCESS:
            let info = state.info;

            info[Object.keys(action.payload)[0]] = Object.values(action.payload)[0];

            return {
                ...state,
                pending: false,
                info
            };

        case FETCH_INFO_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };


        case FETCH_USERS_PENDING:
            return {
                ...state,
                pending: true
            };

        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                pending: false,
                users: action.payload,
                // selectedUser: state.selectedUser ? state.selectedUser : action.payload[0]
            };

        case FETCH_USERS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };

        case TOGGLE_FAV:
            window.localStorage.setItem('onlyFav', !state.onlyFav);

            return {
                ...state,
                onlyFav: !state.onlyFav
            };

        default:
            return state;
    }
}

export default rootReducer;