export default {
    //Users
    users: [],
    selectedUser: window.localStorage.getItem('selectedUser'),

    // Switch config
    branches: [],
    reset: true,
    updKeys: false,
    language: 'null',
    context: 'null',

    //Async stuff
    pending: false,
    info: {},
    error: null,

    // other state props
    onlyFav: window.localStorage.getItem('onlyFav') === 'true'
};