import {fetchUsersError, fetchUsersPending, fetchUsersSuccess} from './index';
import {API_PATH} from "../constants";

function fetchUsers() {
    return dispatch => {

        dispatch(fetchUsersPending());

        fetch(API_PATH + '/users', {
            headers: {
                Accept: 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {

                dispatch(fetchUsersSuccess(res));

                return res;
            })
            .catch(error => {
                dispatch(fetchUsersError(error));
            })
    }
}

export default fetchUsers;
