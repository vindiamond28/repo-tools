import {fetchInfoPending, fetchInfoSuccess, fetchInfoError} from './index';
import store from '../store';
import {API_PATH} from "../constants";

function fetchInfo() {
    let user = store.getState().selectedUser;

    return dispatch => {
        dispatch(fetchInfoPending());
        fetch(API_PATH + '/api.php?user=' + user + '&method=info')
            .then(res => res.json())
            .then(res => {

                dispatch(fetchInfoSuccess({[user]: res}));

                return res;
            })
            .catch(error => {
                dispatch(fetchInfoError(error));
            })
    }
}

export default fetchInfo;