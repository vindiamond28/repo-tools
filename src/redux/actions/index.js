import {
    FETCH_INFO_ERROR,
    FETCH_INFO_PENDING,
    FETCH_INFO_SUCCESS,
    FETCH_USERS_ERROR,
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    SELECT_USER,
    SET_BRANCHES,
    SET_CTX,
    SET_LANG, TOGGLE_FAV,
    TOGGLE_KEYS,
    TOGGLE_RESET
} from "../constants";

export function selectUser(payload) {
    window.localStorage.setItem('selectedUser', payload);

    return {type: SELECT_USER, payload}
}

export function unselectUser() {
    return {type: SELECT_USER, payload: null}
}


export function setBranches(payload) {
    return {type: SET_BRANCHES, payload}
}

export function setLang(payload) {
    return {type: SET_LANG, payload}
}

export function setCtx(payload) {
    return {type: SET_CTX, payload}
}


export function toggleReset() {
    return {type: TOGGLE_RESET}
}

export function toggleKeys() {
    return {type: TOGGLE_KEYS}
}


export function fetchInfoPending() {
    return {
        type: FETCH_INFO_PENDING
    }
}

export function fetchInfoSuccess(info) {
    return {
        type: FETCH_INFO_SUCCESS,
        payload: info
    }
}

export function fetchInfoError(error) {
    return {
        type: FETCH_INFO_ERROR,
        error
    }
}


export function fetchUsersPending() {
    return {
        type: FETCH_USERS_PENDING
    }
}

export function fetchUsersSuccess(info) {
    return {
        type: FETCH_USERS_SUCCESS,
        payload: info
    }
}

export function fetchUsersError(error) {
    return {
        type: FETCH_USERS_ERROR,
        error
    }
}

export function toggleFavourite() {
    return {
        type: TOGGLE_FAV,
    }
}