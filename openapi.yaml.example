#https://swagger.io/docs/specification/describing-parameters/
openapi: 3.0.0

info:
  title: Repository tools
  version: "1.0.0"
  description: |
    Tool for managing Clio's repositories.
  contact:
    name: Team Unicorn

tags:
  - name: users
    description: All endpoints to work with users
  - name: statuses
    description: Returns current repos state
  - name: pipelines
    description: Start a pipeline
  - name: snapshots
    description: Snapshot of repositories for the typed user

paths:
  /users:
    get:
      summary: Returns a list of users.
      #      parameters:
      #        - name: filters
      #          description: Array of filters e.g. ?filters[state]=pending&filters[dueDate]=2019-12-12
      #          in: query
      #          schema:
      #            type: array
      #            items: {}
      #        - name: fields
      #          description: What fields do you want returned e.g. ?fields[0]=state&fields[1]=dueDate
      #          in: query
      #          schema:
      #            type: array
      #            items: {}
      #        - name: limit
      #          description: The limit of your query e.g. ?limit=1
      #          in: query
      #          schema:
      #            type: integer
      #        - name: offset
      #          description: From where should we apply the limit e.g. ?offset=2&limit=1
      #          in: query
      #          schema:
      #            type: integer
      #        - name: sort
      #          description: Which field should the list be sorted after e.g. ?sort=dueDate OR ?sort=-dueDate
      #          in: query
      #          schema:
      #            type: string
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/users'
      tags:
        - users
    post:
      summary: Creates a user.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
              required:
                - name
      responses:
        201:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/users'
        409:
          description: Conflict
      tags:
        - users

  /users/{userId}:
    delete:
      summary: Deletes an item of queue's namespaces.
      parameters:
        - name: userId
          description: User id to delete
          in: path
          required: true
          schema:
            type: string
      responses:
        204:
          description: Successful operation
        500:
          description: Internal Server Error
      tags:
        - users

  /statuses/{timestamp}:
    get:
      parameters:
        - name: timestamp
          description: ...timestamp?
          in: path
          required: true
          schema:
            type: number
      summary: Returns status of pipeline
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/statuses'
      tags:
        - statuses

  /pipelines:
    post:
      summary: Start a pipeline with selected data
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/pipelines'
      responses:
        201:
          description: Successful operation
        500:
          description: ISE
      tags:
        - pipelines

  /snapshots/{timestamp}:
    get:
      summary: Returns snapshot of repositories for the user
      parameters:
        - name: timestamp
          description: fake timestamp
          in: path
          required: true
          schema:
            type: number
        - name: user
          description: user's id
          in: query
          required: true
          schema:
            type: string
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/snapshots
                '
      tags:
        - snapshots

components:
  schemas:
    users:
      type: array
      items:
        type: string
    statuses:
      type: object
      $ref: ''
      properties:
        logs:
          type: string
        status:
          type: string
          enum:
            - pending
            - ready
            - inactive
        snapshot:
          type: object
          properties:
            updated:
              type: number
            groups:
              type: object

    pipelines:
      type: object
      properties:
        user:
          type: string
        branches:
          type: array
          items:
            type: string
        lang:
          type: string
          enum:
            - en
            - dk
            - sv
        ctx:
          type: string
          enum:
            - dk
            - sv
        updKeys:
          type: string
        birds:
          type: array
          items:
            type: string

    snapshots:
      type: object
      properties:
        groups:
          type: object
        timestamp:
          type: integer

servers:
  - description: Test Server URL
    url: http://<user>.cliodev.dk/repo-tools/api/v1
