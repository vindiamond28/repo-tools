Repo Tools
====

## Bootstrapping

1. Clone this repo.

2. Copy `\Configuration\Development\Settings.yaml.example` to `\Configuration\Development\Settings.yaml`.

3. Run `bin/setflowpaths`

## Technial Documentation

You can find the technical documentation in an Open Api Specification format here in the `openapi.yaml` file.

You can also find An HTML generated OAS documentation at `/_documentation`
example : `http://<USER>.cliodev.dk/repo-tools/_documentation/`
