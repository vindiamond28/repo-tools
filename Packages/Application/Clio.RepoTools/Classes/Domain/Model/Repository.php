<?php
namespace Clio\RepoTools\Domain\Model;

use GitWrapper\GitWrapper;
use Neos\Flow\Annotations as Flow;

class Repository
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? 'undefined';
    }

    /**
     * @return string
     */
    public function getDefaultBranch(): string
    {
        // TODO
        return 'master';
    }

    /**
     * @return string
     */
    public function getCurrentBranch(): string
    {
        $gitWrapper = new GitWrapper();

        $gitWrapper = $gitWrapper->workingCopy($this->path);
//        $gitWrapper = $this->gitWrapper->workingCopy($this->path);

        $branches = explode(PHP_EOL, $gitWrapper->branch());

        foreach ($branches as $branch)
        {
            if (strpos($branch, '*') === 0)
            {
                $branch = ltrim($branch, '* ');
                break;
            }
        }

        return $branch ?? 'undefined';
    }
}
