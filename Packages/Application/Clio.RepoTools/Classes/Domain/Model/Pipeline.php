<?php

namespace Clio\RepoTools\Domain\Model;

use Neos\Flow\Annotations as Flow;

class Pipeline
{
    const READY_STATE = 'ready';
    const PENDING_STATE = 'pending';
    const INACTIVE_STATE = 'inactive';

    const PIPELINE_PATH = 'pipeline.dat';
    const DONE_PIPELINE_PATH = 'done_pipeline.log';
    const PENDING_PIPELINE_PATH = 'pending_pipeline.log';

    /**
     * @Flow\InjectConfiguration(package="Clio.RepoTools", path="paths")
     * @var array
     */
    protected $repoToolsPaths;

    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    public function getStatus()
    {
        $this->osUser->init();

        $runnerPath = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getUserId() .
            $this->osUser->getProjectsPath() . '/repo-tools/runner';

        if (!file_exists($runnerPath . '/' . self::PIPELINE_PATH) &&
            !file_exists($runnerPath . '/' . self::PENDING_PIPELINE_PATH)) {
            return self::READY_STATE;
        }

        if (file_exists($runnerPath . '/' . self::PENDING_PIPELINE_PATH)) {
            return self::PENDING_STATE;
        }

        return self::INACTIVE_STATE;
    }

    /**
     * Returns current or previous pipeline logs
     * @param string $state
     * @return string
     */
    public function getLog($state = self::READY_STATE): string
    {
        $this->osUser->init();

        $runnerPath = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getUserId() .
            $this->osUser->getProjectsPath() . '/repo-tools/runner';

        switch ($state) {
            case self::READY_STATE:
                return file_exists($runnerPath . '/' . self::DONE_PIPELINE_PATH) ?
                    file_get_contents($runnerPath . '/' . self::DONE_PIPELINE_PATH) :
                    '';

            case self::PENDING_STATE:
                return file_exists($runnerPath . '/' . self::PENDING_PIPELINE_PATH) ?
                    file_get_contents($runnerPath . '/' . self::PENDING_PIPELINE_PATH) :
                    '';

            default:
                return '<span style="color: red">Error!</span> Please, go to the project directory and run <code>runner.sh</code>';
        }
    }
}
