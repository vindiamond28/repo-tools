<?php

namespace Clio\RepoTools\Domain\Model;

use Neos\Flow\Annotations as Flow;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class Snapshot
{
    const SNAPSHOT_FILE = 'snapshot.yaml';

    /**
     * @Flow\InjectConfiguration(package="Clio.RepoTools", path="paths")
     * @var array
     */
    protected $repoToolsPaths;

    public function getSnapshotByUser(OsUser $osUser): array
    {
        $snapshotPath = $this->repoToolsPaths['homeDir'] . '/' . $osUser->getCurrentOsUser() .
            $osUser->getProjectsPath() . '/repo-tools/runner/' . $osUser->getUserId() . '_' . Snapshot::SNAPSHOT_FILE;

        if (!file_exists($snapshotPath)) {
            return [];
        }

        try {
            return Yaml::parse(file_get_contents($snapshotPath));
        } catch (ParseException $exception)
        {
            return [];
        }
    }

    public function getSnapshotsTimestampByUser(OsUser $osUser): int
    {
        $snapshotPath = $this->repoToolsPaths['homeDir'] . '/' . $osUser->getCurrentOsUser() .
            $osUser->getProjectsPath() . '/repo-tools/runner/' . $osUser->getUserId() . '_' . Snapshot::SNAPSHOT_FILE;

        if (!file_exists($snapshotPath)) {
            return 0;
        }

        return filemtime($snapshotPath);
    }
}
