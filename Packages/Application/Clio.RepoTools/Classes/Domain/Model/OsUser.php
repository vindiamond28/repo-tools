<?php

namespace Clio\RepoTools\Domain\Model;

use Neos\Flow\Annotations as Flow;

class OsUser
{
    /**
     * @Flow\InjectConfiguration(package="Clio.RepoTools", path="paths")
     * @var array
     */
    protected $repoToolsPaths;

    /**
     * @var
     */
    protected $userId = '';

    public function init($userId = null)
    {
        $this->userId = trim($userId ?? $this->getCurrentOsUser());
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    public function isExist(): bool
    {
        return !empty($this->userId) && file_exists($this->repoToolsPaths['homeDir'] . '/' . $this->userId);
    }

    public function getProjectsPath(): string
    {
        return $this->repoToolsPaths['projectsPath'][$this->userId] ?? $this->repoToolsPaths['projectsPath']['default'];
    }

    /**
     * Returns user who ran the app
     * @return string
     */
    public function getCurrentOsUser(): string
    {
        return explode(
            '/',
            substr(
                getcwd(),
                strlen($this->repoToolsPaths['homeDir']) + 1
            )
        )[0];
    }
}
