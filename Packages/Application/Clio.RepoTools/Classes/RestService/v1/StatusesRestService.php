<?php

namespace Clio\RepoTools\RestService\v1;

use Clio\RepoTools\Domain\Model\OsUser;
use Clio\RepoTools\Domain\Model\Pipeline;
use ClioOnline\DomainClient\Http\Exception\BadRequestException;
use ClioOnline\DomainClient\Rest\RestJsonResult;
use ClioOnline\FlowDomainServer\RestService\AbstractRestService;
use Neos\Flow\Annotations as Flow;

class StatusesRestService extends AbstractRestService
{
    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    /**
     * @var Pipeline
     * @Flow\Inject
     */
    protected $pipeline;

    /**
     * @Flow\Inject
     * @var \Clio\RepoTools\Service\ConfigService
     */
    protected $configService;

    public function getResourceName(): ?string
    {
        return 'statuses';
    }

    public function show(string $id, array $fields = [], array $arguments = []): RestJsonResult
    {
        $this->osUser->init($arguments['user-id'] ?? '');

        if (!$this->osUser->isExist()) {
            throw new BadRequestException("User '" . $this->osUser->getUserId() . "' doesn't exist");
        }

        $pipelineState = $this->pipeline->getStatus();

        return new RestJsonResult([
            'logs' => $this->pipeline->getLog($pipelineState),
            'status' => $pipelineState,
            'snapshot' => [
                'updated' => 65756474,
                'groups' => $this->configService->getRepositories($this->osUser)
            ]
        ]);
    }
}
