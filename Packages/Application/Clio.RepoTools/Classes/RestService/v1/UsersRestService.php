<?php

namespace Clio\RepoTools\RestService\v1;

use Clio\RepoTools\Domain\Model\OsUser;
use ClioOnline\DomainClient\Http\Exception\BadRequestException;
use ClioOnline\DomainClient\Rest\RestJsonCreateResult;
use ClioOnline\DomainClient\Rest\RestJsonDeleteResult;
use Neos\Flow\Annotations as Flow;
use ClioOnline\DomainClient\Rest\RestJsonListResult;
use ClioOnline\FlowDomainServer\RestService\AbstractRestService;

class UsersRestService extends AbstractRestService
{
    /**
     * @Flow\Inject
     * @var \Clio\RepoTools\Service\ConfigService
     */
    protected $configService;

    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    /**
     * @return string|null
     */
    public function getResourceName(): ?string
    {
        return 'users';
    }

    public function list(array $filters = [], array $fields = [], int $limit = 20, int $offset = 0, string $sort = null): RestJsonListResult
    {
        return new RestJsonListResult($this->configService->getUsers());
    }

    public function delete(string $resourceId): RestJsonDeleteResult
    {
        $this->osUser->init($resourceId ?? '');

        if (!$this->osUser->isExist()) {
            throw new BadRequestException("User '" . $this->osUser->getUserId() . "' doesn't exist");
        }

        return new RestJsonDeleteResult(
            [],
            $this->configService->deleteUser($this->osUser) ?
                204 :
                500);
    }

    public function create(array $data = []): RestJsonCreateResult
    {
        $this->osUser->init($data['name'] ?? '');

        return new RestJsonCreateResult((array)$this->configService->addUser($this->osUser));
    }
}
