<?php

namespace Clio\RepoTools\RestService\v1;

use Clio\RepoTools\Domain\Model\OsUser;
use Clio\RepoTools\Domain\Model\Snapshot;
use ClioOnline\DomainClient\Http\Exception\BadRequestException;
use ClioOnline\DomainClient\Rest\RestJsonResult;
use ClioOnline\FlowDomainServer\RestService\AbstractRestService;
use Neos\Flow\Annotations as Flow;

class SnapshotsRestService extends AbstractRestService
{
    /**
     * @var Snapshot
     * @Flow\Inject
     */
    protected $snapshot;

    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    public function getResourceName(): ?string
    {
        return 'snapshots';
    }

    public function show(string $id, array $fields = [], array $arguments = []): RestJsonResult
    {
        $this->osUser->init($arguments['user'] ?? '');

        if (!$this->osUser->isExist()) {
            throw new BadRequestException("User '" . $this->osUser->getUserId() . "' doesn't exist");
        }

        return new RestJsonResult([
            'groups' => $this->snapshot->getSnapshotByUser($this->osUser),
            'timestamp' => $this->snapshot->getSnapshotsTimestampByUser($this->osUser)
        ]);
    }
}

