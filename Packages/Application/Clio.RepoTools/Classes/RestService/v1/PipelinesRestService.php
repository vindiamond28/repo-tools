<?php

namespace Clio\RepoTools\RestService\v1;

use Clio\RepoTools\Domain\Model\OsUser;
use Clio\RepoTools\Domain\Model\Pipeline;
use ClioOnline\DomainClient\Rest\RestJsonCreateResult;
use ClioOnline\FlowDomainServer\RestService\AbstractRestService;
use Neos\Flow\Annotations as Flow;

class PipelinesRestService extends AbstractRestService
{
    /**
     * @Flow\InjectConfiguration(package="Clio.RepoTools", path="paths")
     * @var array
     */
    protected $repoToolsPaths;

    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    const STAGE_GATHER_REPOS = 0;
    const STAGE_CHECKOUT_REPOS = 1;
    const STAGE_CONTEXT = 2;
    const STAGE_LANGUAGE = 3;
    const STAGE_TRANSLATION = 4;
    const STAGE_YAKINTOS_LINK = 5;
    const STAGE_YAKINTOS_BUILD = 6;
    const STAGE_CACHE = 7;
    const STAGE_SNAPSHOT = 8;

    const STAGE_NAMES = [
        'Gather user\'s repositories',
        'Git reset, fetch, checkout',
        'Switch context',
        'Switch language',
        'Switch translation',
        'Yakintos link',
        'Yakintos build, watch',
        'Flow flush cache',
        'Make snapshot'
    ];

    public function getResourceName(): ?string
    {
        return 'pipelines';
    }

    public function create(array $data = []): RestJsonCreateResult
    {
        $commands = '';
        $key = 1;

        // iteration through stages
        foreach (self::STAGE_NAMES as $stageKey => $stageName)
        {
            $echo = "printf \"<b>Stage {$key}</b>: <i>{$stageName}:</i> \\n\\n\" && \n";

            switch ($stageKey)
            {
                case self::STAGE_GATHER_REPOS:
                    if (!empty($data['gather-repos'])) {
                        $commands .= $echo . "./flow repos:gather --user-id {$data['user']} && \n";
                    }
                    $key++;

                    break;
//user: this.props.selectedUser,
//    // reset: this.props.reset,
//    // prefetch: this.props.prefetch,
//branches: data,
//lang: this.props.language,
//ctx: this.props.context,
//updKeys: this.props.updKeys,
//birds: this.state.birds
//                case self::STAGE_CHECKOUT_REPOS:
//                    break;
//
//                case self::STAGE_CONTEXT:
//                    break;
//
//                case self::STAGE_LANGUAGE:
//                    break;
//
//                case self::STAGE_TRANSLATION:
//                    break;
//
//                case self::STAGE_YAKINTOS_LINK:
//                    break;
//
//                case self::STAGE_YAKINTOS_BUILD:
//                    break;
//
//                case self::STAGE_CACHE:
//                    break;

                case self::STAGE_SNAPSHOT:
                    $commands .= $echo . "./flow repos:snapshot --user-id {$data['user']}\n";
                    break;
            }
        }

        $this->osUser->init();

        $runnerPath = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getUserId() .
            $this->osUser->getProjectsPath() . '/repo-tools/runner';

        file_put_contents($runnerPath . '/' . Pipeline::PIPELINE_PATH, $commands);

        return new RestJsonCreateResult(['path' => $runnerPath . '/' . Pipeline::PIPELINE_PATH]);
    }
}
