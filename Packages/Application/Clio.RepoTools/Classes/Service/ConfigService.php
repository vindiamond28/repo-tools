<?php

namespace Clio\RepoTools\Service;

use Clio\RepoTools\Domain\Model\OsUser;
use Clio\RepoTools\Domain\Model\Repository;
use Clio\RepoTools\Helper\Helper;
use ClioOnline\DomainClient\Http\Exception\BadRequestException;
use Neos\Flow\Annotations as Flow;
use Symfony\Component\Yaml\Yaml;

class ConfigService
{
    const RESOURCE_PATH = 'resource://Clio.RepoTools/Private/Config.yaml';

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        if (file_exists(self::RESOURCE_PATH)) {
            $data = file_get_contents(self::RESOURCE_PATH);
        }

        return empty($data) ?
            [] :
            Yaml::parse($data) ?? [];
    }

    protected function setConfig(array $data): bool
    {
        return file_put_contents(self::RESOURCE_PATH, Yaml::dump($data, 12, 2));
    }

    /**
     * @return array|mixed
     */
    public function getUsers(): array
    {
        $config = $this->getConfig();

        return empty($config['users']) ?
            [] :
            $config['users'];
    }

    /**
     * @param OsUser $osUser
     * @param bool $unshift
     * @return string|null
     */
    public function addUser(OsUser $osUser, $unshift = false): ?string
    {
        if (!$osUser->isExist()) {
            throw new BadRequestException("User with name '" . $osUser->getUserId() . "' doesn't exist in a system", 400);
        }

        $config = $this->getConfig();

        $config['users'] = $config['users'] ?? [];

        $unshift ?
            array_unshift($config['users'], $osUser->getUserId()) :
            array_push($config['users'], $osUser->getUserId());

        $config['users'] = array_values(array_unique($config['users']));

        $this->setConfig($config);

        return $config['users'][array_search($osUser->getUserId(), $config['users'])];
    }

    /**
     * @param OsUser $osUser
     * @return bool
     */
    public function deleteUser(OsUser $osUser): bool
    {
        $config = $this->getConfig();

        if (in_array($osUser->getUserId(), $config['users'])) {
            unset($config['users'][array_search($osUser->getUserId(), $config['users'])]);

            $config['users'] = array_values($config['users']);
        }

        return $this->setConfig($config);
    }

    public function isProjectPathAlreadyInConf(string $name, OsUser $osUser): bool
    {
        $config = $this->getConfig();

        if (empty($config['repos'][$osUser->getUserId()])) {
            return false;
        }

        return Helper::inArrayRecursive($name, $config['repos'][$osUser->getUserId()]);
    }

    /**
     * @param Repository $repository
     * @param OsUser $osUser
     * @param string $group
     */
    public function addRepository(Repository $repository, OsUser $osUser, $group = 'other')
    {
        $config = $this->getConfig();

        $config['repos'][$osUser->getUserId()][$group][$repository->getPath()] = [
            'name' => $repository->getName(),
            'default-branch' => $repository->getDefaultBranch()
        ];

        $this->setConfig($config);
    }

    /**
     * @param OsUser $osUser
     * @return array
     */
    public function getRepositories(OsUser $osUser): array
    {
        $config = $this->getConfig();

        return $config['repos'][$osUser->getUserId()] ?? [];
    }
}
