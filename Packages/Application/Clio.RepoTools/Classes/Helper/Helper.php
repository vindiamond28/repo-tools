<?php

namespace Clio\RepoTools\Helper;

class Helper
{
    /**
     * Checks if the $haystack array consists $needle
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @return bool
     */
    static function inArrayRecursive($needle, $haystack, $strict = false) : bool
    {
        foreach ($haystack as $repoPath => $data) {
            if (($strict ? $repoPath === $needle : $repoPath == $needle) || (is_array($data) && self::inArrayRecursive($needle, $data, $strict))) {
                return true;
            }
        }

        return false;
    }
}
