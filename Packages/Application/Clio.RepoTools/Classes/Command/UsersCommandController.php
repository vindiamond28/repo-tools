<?php

namespace Clio\RepoTools\Command;

use Clio\RepoTools\Domain\Model\OsUser;
use Neos\Flow\Cli\CommandController;
use Neos\Flow\Annotations as Flow;

class UsersCommandController extends CommandController
{
    /**
     * @Flow\Inject
     * @var \Clio\RepoTools\Service\ConfigService
     */
    protected $configService;

    /**
     * @Flow\Inject
     * @var OsUser
     */
    protected $osUser;

    /**
     * Inits main user
     *
     * This command adds current user to the config
     */
    public function initCommand()
    {
        $this->osUser->init();

        $this->configService->addUser(
            $this->osUser,
            true
        );
    }
}
