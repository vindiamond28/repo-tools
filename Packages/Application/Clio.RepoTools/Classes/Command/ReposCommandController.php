<?php

namespace Clio\RepoTools\Command;

use Clio\RepoTools\Domain\Model\OsUser;
use Clio\RepoTools\Domain\Model\Snapshot;
use Neos\Flow\Cli\CommandController;
use Neos\Flow\Annotations as Flow;
use Symfony\Component\Yaml\Yaml;

class ReposCommandController extends CommandController
{
    /**
     * @Flow\InjectConfiguration(package="Clio.RepoTools", path="paths")
     * @var array
     */
    protected $repoToolsPaths;

    /**
     * @var OsUser
     * @Flow\Inject
     */
    protected $osUser;

    /**
     * @Flow\Inject
     * @var \Clio\RepoTools\Service\ConfigService
     */
    protected $configService;

    /**
     * @Flow\Inject
     * @var \Clio\RepoTools\Domain\Model\Repository
     */
    protected $repository;

    /**
     * Gathers paths of all the repositories
     *
     * This command searches all the repositories in the projects path from configuration
     * @param string $userId
     */
    public function gatherCommand($userId = null)
    {
        $this->osUser->init($userId);

        if (!$this->osUser->isExist()) {
            $this->outputLine("User '" . $this->osUser->getUserId() . "' doesn't exist");

            return;
        }

        $projectsDir = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getUserId() .
            $this->osUser->getProjectsPath();

        // collect repository paths
        // TODO add additional paths to find repositories
        $repoPaths = array_merge(
            $this->getGits($projectsDir)
//            $this->getGits($pathToProjects . "/flow/Packages/Application"),
//            $this->getGits($pathToProjects . "/abo2/Packages/Application")
        );

        // trim the path to projects dir
        $trimLength = strlen($projectsDir) + 1;

        foreach ($repoPaths as &$repoPath)
        {
            $repoPath = substr($repoPath, $trimLength);
        }

        sort($repoPaths, SORT_STRING);

        $newRepos = 0;

        // append new repositories to the config
        foreach ($repoPaths as $repoPath) {
            if (!$this->configService->isProjectPathAlreadyInConf($repoPath, $this->osUser)) {

                $this->repository->setPath($repoPath);

                $this->configService->addRepository($this->repository, $this->osUser);

                $newRepos++;
            }
        }

        $newRepos ?
            $this->outputLine($newRepos .' repositories were added') :
            $this->outputLine('No new repositories');
    }

    /**
     * Returns an array of directories with repos
     * @param string $dir Root of search
     * @param array $accum Accumulates results
     * @return array
     */
    protected function getGits($dir, $accum = []): array
    {
        foreach (scandir($dir) as $file) {
            if (is_dir($dir . "/" . $file) && $file !== '.' && $file !== '..') {
                if ($file === '.git') {
                    $accum[] = $dir;
                    break;
                } else {
                    $accum = array_unique(array_merge($accum, $this->getGits($dir . "/" . $file, $accum)));
                }
            }
        }

        return $accum;
    }

    /**
     * Collects info about all the repositories
     *
     * This command searches all the repositories in the projects path from configuration
     * @param string $userId
     */
    public function snapshotCommand($userId = null)
    {
        $this->osUser->init($userId);

        if (!$this->osUser->isExist()) {
            $this->outputLine("User '" . $this->osUser->getUserId() . "' doesn't exist");

            return;
        }

        // gathering repos info
        $repositories = $this->configService->getRepositories($this->osUser);

        $projectsDir = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getUserId() .
            $this->osUser->getProjectsPath();

        $snapshot = [];

        foreach ($repositories as $group => $repository) {
            foreach ($repository as $path => $data) {
                $this->repository->setPath($projectsDir . '/' . $path);

                $snapshot[$group][$path] = [
                    'branch' => $this->repository->getCurrentBranch()
                ];
            }
        }

        $mainUserProjectsDir = $this->repoToolsPaths['homeDir'] . '/' . $this->osUser->getCurrentOsUser() .
            $this->osUser->getProjectsPath() . '/repo-tools/runner';

        file_put_contents($mainUserProjectsDir . '/' . $this->osUser->getUserId() . '_' . Snapshot::SNAPSHOT_FILE, Yaml::dump($snapshot, 12, 2));
    }
}
