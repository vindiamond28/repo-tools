#!/bin/sh

doPipeline()
{
  # prevent processing other instances of runner
  if [ -f ./runner/pending_pipeline.log ]; then

    printf "[error] there is already working runner (if it isn't true remove pending_pipeline.log)\n"

    exit 1

  fi

  # if there is a pending pipeline
  if [ -f ./runner/pipeline.dat ]; then

    # executing pipeline
    sh ./runner/pipeline.dat > ./runner/pending_pipeline.log

    # remove after work is done
    mv -f ./runner/pending_pipeline.log ./runner/done_pipeline.log

    rm -f ./runner/pipeline.dat

    printf "[info] pipeline finished. See details in done_pipeline.log\n"

  fi
}

loop()
{
  doPipeline

  sleep 5

  loop
}

printf "[ok] runner successfully started\n"

# defining current user
./flow users:init

# gathering new repos
./flow repos:gather

loop
